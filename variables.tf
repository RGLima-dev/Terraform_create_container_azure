variable "location_resource_group"{
    description = "Location where the resources group will be instanciaded"
    type = string
    default = "East US"
}

variable "location_storage_acc"{
    description = "Location where the storage account will be instanciaded"
    type = string
    default = "West US"
}

variable "tier_storage_account"{
    description = "tier of storage account"
    type = string
    default = "Standard"
}

variable "container_access_type"{
    description = "Access type of the container"
    type = string
    default = "container"
}