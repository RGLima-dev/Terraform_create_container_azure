resource "azurerm_resource_group" "rg-terraform-study" {
  name     = "terraform-study-azure"
  location = var.location_resource_group
}

resource "azurerm_storage_account" "storageacc-terraform" {
  name                     = "rodrigolimaterraform"
  resource_group_name      = azurerm_resource_group.rg-terraform-study.name
  location                 = var.location_storage_acc
  account_tier             = var.tier_storage_account
  account_replication_type = "GRS"

  tags = {
    environment = "dev"
    company = "Programmers"
  }
}

resource "azurerm_storage_container" "containerc1" {
name                  = "containernumberone"
storage_account_name  = azurerm_storage_account.storageacc-terraform.name
container_access_type = var.container_access_type
}

